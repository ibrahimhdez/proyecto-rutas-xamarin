﻿using System;

using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Threading.Tasks;
using System.Net.Http;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using System.Collections;
using Xamarin.Essentials;
using System.Linq;

namespace Mapa
{
    public partial class MainPage : ContentPage 
    {
        private ArrayList routePoints;

        public MainPage()
        {
            Debug.WriteLine("YAS");
            InitializeComponent();
            Content = MapView;

            routePoints = new ArrayList();
        }

        protected override async void OnParentSet()
        {
            Debug.WriteLine("QUE");
            var address = "Calle Piamonte 9";
            var locations = await Geocoding.GetLocationsAsync(address);
            var location = locations?.FirstOrDefault();
            var originWayPoint = $"{location.Latitude.ToString().Replace(",", ".")},{location.Longitude.ToString().Replace(",", ".")}";

            var addressDest = "Plaza de Chueca";
            var destinationLocations = await Geocoding.GetLocationsAsync(addressDest);
            var destinationLocation = destinationLocations?.FirstOrDefault();
            var destinationWayPoint = $"{destinationLocation.Latitude.ToString().Replace(",",".")},{destinationLocation.Longitude.ToString().Replace(",", ".")}";

           // await GetRouteJSON(originWayPoint, destinationWayPoint);
        }

        private async Task GetRouteJSON(string origin, string destination)
        {
            HttpClient client = new HttpClient();
            string Url = $"http://dev.virtualearth.net/REST/v1/Routes?wp.0={origin}&wp.1={destination}&routePathOutput=Points&tl=1.0&key=AvGdGjyf5hLeAEZQB0ajThVfKuTIq4mafxysAn6E8xO9q1FraWoG9XiLawmzIbqM";
            Debug.WriteLine(Url);
            string content = await client.GetStringAsync(Url);

            JObject json = JObject.Parse(content);
            JArray values = (JArray)json["resourceSets"];
            var resources = (JArray)values[0].ToObject<JObject>()["resources"];
            var routePath = resources[0].ToObject<JObject>()["routePath"];
            var line = routePath["line"];
            var coordinates = (JArray)line["coordinates"];
           
            GetPointsArray(coordinates);
            SetRouteAsync();
        }

        private void GetPointsArray(JArray coordinates)
        {
            foreach (var coordinate in coordinates.Children())
            {
                routePoints.Add(new Position(coordinate[0].ToObject<double>(), coordinate[1].ToObject<double>()));
            }
        }

        private void  SetRouteAsync()
        {
            foreach (Position pointPosition in routePoints)
            {
       //         MapView.RouteCoordinates.Add(pointPosition);
            }

     //       MapView.MoveToRegion(MapSpan.FromCenterAndRadius((Position)routePoints[routePoints.Count - 1], Distance.FromMeters(1500)));
          //  MapView.ChangeProperty();
        }

        public async Task GrantPermissions()
        {
            await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
        }

     //   async Task GetCurrentLocation()
      //  {
         //   var locator = CrossGeolocator.Current;
      //      var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(1));
     //       MapView.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude),
                                      //       Distance.FromMiles(1)));
       // }

        private void Street_OnClicked(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Street;
        }


        private void Hybrid_OnClicked(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Hybrid;

        }

        private void Satellite_OnClicked(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Satellite;
        } 
    }
}
